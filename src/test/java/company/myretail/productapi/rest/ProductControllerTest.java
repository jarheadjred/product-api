package company.myretail.productapi.rest;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;

import company.myretail.productapi.repository.Money;
import company.myretail.productapi.repository.ProductPriceFragment;
import company.myretail.productapi.repository.ProductPriceFragmentRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ProductPriceFragmentRepository mockPriceFragmentRepository;

    @Test
    public void testGetNoResult() {
        webTestClient.get()
        .uri("/products/{id}", "no_product_here")
        .exchange()
        .expectStatus()
        .isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void testProductOneExists() {
        // given
        String productId = "000001";
        ProductPriceFragment pf = new ProductPriceFragment(productId, new Money("USD", BigDecimal.ONE));
        Mockito.when(mockPriceFragmentRepository.findById( productId )).thenReturn(Optional.of(pf));

        webTestClient.get()
        .uri("/products/{id}", productId)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
            .json("{\"id\":\"000001\",\"name\":\"Hardcoded Product Name\",\"currentPrice\":{\"currency\":\"USD\",\"value\":1}}");
        //.expectBody(ProductModel.class).isEqualTo(new ProductModel(pf));
        
    }
}