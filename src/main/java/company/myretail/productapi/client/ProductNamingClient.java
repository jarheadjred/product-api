package company.myretail.productapi.client;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component
public class ProductNamingClient {
    
    private final WebClient http;

    public ProductNamingClient(WebClient http) {
        this.http = http;
    }

    // FIXME: change this to fetch using the http client and setup a Wiremock server with expecteed responses
    public String getProductName(String id) {
        return "Hardcoded Product Name";
    }
}