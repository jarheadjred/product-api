package company.myretail.productapi.rest;

import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import company.myretail.productapi.repository.Money;
import company.myretail.productapi.repository.ProductPriceFragment;

public class ProductModel implements Serializable {

	private static final long serialVersionUID = -6045629879239878451L;
	private final Money currentPrice;
	private final String name;
	private String id;

	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
	public ProductModel(@JsonProperty("id") String id, @JsonProperty("currentPrice") Money currentPrice, @JsonProperty("name")String name) {
		this.id = id;
		this.currentPrice = currentPrice;
		this.name = name;
	}

	public ProductModel(String id, ProductPriceFragment pf, String name) {
		this.id = id;
		this.currentPrice = pf.getPrice();
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public Money getCurrentPrice() {
		return currentPrice;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof ProductModel)) {
			return false;
		}
		ProductModel productModel = (ProductModel) o;
		return Objects.equals(currentPrice, productModel.currentPrice) && Objects.equals(name, productModel.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(currentPrice, name);
	}


	@Override
	public String toString() {
		return "{" +
			" currentPrice='" + getCurrentPrice() + "'" +
			", name='" + getName() + "'" +
			", id='" + getId() + "'" +
			"}";
	}
	

}
