package company.myretail.productapi.rest;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import company.myretail.productapi.client.ProductNamingClient;
import company.myretail.productapi.repository.ProductPriceFragment;
import company.myretail.productapi.repository.ProductPriceFragmentRepository;
import reactor.core.publisher.Mono;

@RestController
public class ProductController {
    
    private final ProductPriceFragmentRepository productPriceFragmentRepository;
    private final ProductNamingClient nameClient;

    public ProductController(ProductPriceFragmentRepository productPriceFragmentRepository, ProductNamingClient client) {
        this.productPriceFragmentRepository = productPriceFragmentRepository;
        this.nameClient = client;
    }
    @GetMapping("/products/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ProductModel> getProduct(@PathVariable(required = true) String id) {

        // FIXME: as we adjust to purely reactive and functional, ensure that the id is handled correctly as a Mono and chain down from there

        Optional<ProductPriceFragment> ppf = productPriceFragmentRepository.findById(id);
        
        ProductModel productModel = new ProductModel(id,
                ppf.orElseThrow(() -> new ResponseStatusException(HttpStatus.NO_CONTENT,
                        String.format("No product with id of {}", id))),
                nameClient.getProductName(id));
        
        return Mono.just(productModel);
        
        // TODO: ideally we make the whole call chain fully reactive. It will look something like this
        // return Mono.just(id)
        //     .flatMap(productPriceFragmentRepository::findById )
        //     .flatMap(ppfOptional ->  Mono.just(ppfOptional.map(ProductModel::new)) )
        //     .switchIfEmpty());
    }

    
}