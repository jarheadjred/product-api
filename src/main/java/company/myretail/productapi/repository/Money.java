package company.myretail.productapi.repository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Money implements Serializable {

    private static final long serialVersionUID = 7709622538380761606L;

    // FIXME: either refactor to enum or just use JSR 354
    private final String currency;
    private final BigDecimal value;

	@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public Money(@JsonProperty("currency") String currency, @JsonProperty("value") BigDecimal value) {
        this.currency = currency;
        this.value = value;
    }
    
    public String getCurrency() {
        return this.currency;
    }

    public BigDecimal getValue() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Money)) {
            return false;
        }
        Money money = (Money) o;
        return Objects.equals(currency, money.currency) && Objects.equals(value, money.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency, value);
    }

    @Override
    public String toString() {
        return "{" + " currency='" + getCurrency() + "'" + ", value='" + getValue() + "'" + "}";
    }

}