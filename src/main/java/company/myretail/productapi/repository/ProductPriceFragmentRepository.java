package company.myretail.productapi.repository;

import org.springframework.data.repository.CrudRepository;

public interface ProductPriceFragmentRepository extends CrudRepository<ProductPriceFragment, String> {
    // FIXME: convert from CrudRepository to reactive model to support fully nonblocking usage
    
}