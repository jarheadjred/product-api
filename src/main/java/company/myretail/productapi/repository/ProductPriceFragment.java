package company.myretail.productapi.repository;

import java.util.Objects;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("product_price")
public class ProductPriceFragment {

    @Id
    private String id;
    private Money price;

    public ProductPriceFragment(String id, Money price) {
        this.price = price;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public ProductPriceFragment() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProductPriceFragment id(String id) {
        this.id = id;
        return this;
    }

    public ProductPriceFragment price(Money price) {
        this.price = price;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ProductPriceFragment)) {
            return false;
        }
        ProductPriceFragment productPriceFragment = (ProductPriceFragment) o;
        return Objects.equals(id, productPriceFragment.id) && Objects.equals(price, productPriceFragment.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price);
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", price='" + getPrice() + "'" + "}";
    }

}