# MyRetail: product-api

[![build-status badge](https://gitlab.com/jarheadjred/product-api/badges/master/pipeline.svg
)](https://gitlab.com/jarheadjred/product-api/pipelines) 
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme) 
TODO: Put more badges here.

The My Retail Product API is an aggregating service that allows consumers to retrieve and update Product representations for shopping experiences.

This project is a Spring Boot Java application. It follows standard modern (as of 05/2020) gradle conventions for building and packaging artifacts. 

It uses Architectural Decision Records (ADRs) called out in the Markdown ADR references (https://adr.github.io/madr/) 
In the ADR we capture the rational for why certain architectural choices have been selected (where architecture here is defined as the things that are difficult to change as the project reaches maturity).
*  Why was a Design Pattern selected for the application
*  Why was a particular data store chosen, has a pattern been put in place to mitigate churn if it needs to change?
*  Nearly any other why associated with ***BIG ROCK*** related issues that are critical to the application's capabilities and it's "identitiy"

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

Original Project Description and Requirements
### myRetail RESTful service

myRetail is a rapidly growing company with HQ in Richmond, VA and over 200 stores across the east coast. myRetail wants to make its internal data available to any number of client devices, from myRetail.com to native mobile apps. 

The goal for this exercise is to create an end-to-end Proof-of-Concept for a products API, which will aggregate product data from multiple sources and return it as JSON to the caller. 

Your goal is to create a RESTful service that can retrieve product and price details by ID. The URL structure is up to you to define, but try to follow some sort of logical convention.

Build an application that performs the following actions: 
- Responds to an HTTP GET request at /products/{id} and delivers product data as JSON (where {id} will be a number. 
- Example product IDs: 15117729, 16483589, 16696652, 16752456, 15643793) 
- Example response: 
```
{"id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","current_price":{"value": 13.49,"currency_code":"USD"}}
```
- Performs an HTTP GET to retrieve the product name from an external API. (For this exercise the data will come from redsky.target.com, but let’s just pretend this is an internal resource hosted by myRetail)
- Example: http://redsky.target.com/v2/pdp/tcin/13860428?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics
- Reads pricing information from a NoSQL data store and combines it with the product id and name from the HTTP request into a single response.
- BONUS: Accepts an HTTP PUT request at the same path (/products/{id}), containing a JSON request body similar to the GET response, and updates the product’s price in the data store.

## Install
This project is primarily a standard Spring Boot application created initially using the https://start.spring.io initializr.

It depends on having a JDK on your computers classpath. For convenience, we use SDK Man to configure our JDK.

The project itself utilizes gradle wrapper and as long as you have the JDK on your computers path then this should build the project.
```
./gradlew build
```

In order to provide external service dependencies in the most convenient way possible the project uses Docker to run and manage various services. We use Brew package manager to easily install and manage Docker on our Mac. Assuming that Docker is installed you should be able to start the projects dependancies using the following command.
```
```

## Usage

The Product API application is enabled with Operations endpoints and a variety of other management features. 

To run the application there is a dependency on Redis server. To easily get running on your machine execute the following:
```
docker-compose up -d
```
That will get a local redis instance available on port `6379`

With Redis running you can start the app:
``` 
./gradlew bootRun
```

With the application running you can access the path `/actuator` to see a listing of the endpoints
```
curl localhost:8080/actuator
```

And go ahead and curl for a product:"
``` 
curl localhost:8080/products/123
```


## API

My Retail product-api has a REST interface for consumers. 

Resource:
/product

GET /product/{id}
Response Body
```
{"id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","current_price":{"value": 13.49,"currency_code":"USD"}}
```

PUT /product/{id}
Request Body
```
{ "id": "value", "current_price":{"value": 13.49,"currency_code":"USD"}
```

## Maintainers

[@https://gitlab.com/jarheadjred](https://github.com/https://gitlab.com/jarheadjred)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Jared Olhoft
