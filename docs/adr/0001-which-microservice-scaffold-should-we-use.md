# Use Spring Boot Reactive Rest for Service

* Status: [accepted] <!-- optional -->
* Deciders: jared.olhoft@gmail.com] <!-- optional -->
* Date: [2020-05-06] <!-- optional -->

<!-- Technical Story: [description | ticket/issue URL]  optional -->

## Context and Problem Statement

We need to create a Rest API that will aggreggate information from at least two different sources. One, a NoSql Datastore private to this service. Two, a third-part rest service that provides some product information. We expect that My Retail will have very high load and will need to be built in a way that will be amenable to horizontal scaling without requiring large vertically scaled instances. What frameworks are candidates for a self-hosted Rest Service?

## Decision Drivers <!-- optional -->

* [driver 1, force: human capital and talents, do our existing engineers have skills in the laguage and available sdks]
* [driver 2, company platform recommendations, are we taking the paved road or blazing our own trail, …]
* … <!-- numbers of drivers can vary -->

## Considered Options

* [go-kit](https://gokit.io/)
* [Spring WebMVC](https://spring.io/guides/gs/serving-web-content/)
* [Spring WebFlux](https://spring.io/guides/gs/reactive-rest-service/)
* … <!-- numbers of options can vary -->

## Decision Outcome

Chosen option: "Spring WebFlux", because WebFlux runs as a standard Spring Boot application and is nonblocking. This satisfies the platform runtime support of running "just another spring boot app." In addition, the WebFlux integrations and API's have come a long way to fully integrate and support common Spring WebMVC style coding. Finally, by running a reactive stack, we are non-blocking from the incoming request all the way down to the wire-related calls going out (assuming we choose our NoSql wisely).

### Positive Consequences <!-- optional -->

* Java language and spring boot are well known to the software and platform engineers
* NonBlocking reactive models allow for higher throughput without extra resource overhead of traditional threading models. 
* Resillient principals and null safety/handling built into the core features of the sdk

### Negative Consequences <!-- optional -->

* Running on the JVM, larger artifacts, container runtime much bigger than needed
* Building with gradle & jvm, not as fast as other possible choices, especially in CI/CD environments

<!-- ## Pros and Cons of the Options <!-- optional -->

### [option 1]

[[go-kit](https://gokit.io/) | golang based microservices framework ] <!-- optional -->

* Good, because compile time and runtime can be optimized to platform, very small and fast
* Good, starts to socialize golang to more of the engineering org, familiarity ties to k8s and other industry momentum
* Bad, because existing software engineering culture isn't familiar with language and conventions
* Bad, we'll need to blaze the new trail for CI/CD, testing, observability (Log, Metrics) confirguation and reporting, container build, etc. Many new frameworks, and general infrastructure to learn, implement, and support

<!-- ### [option 2]

[example | description | pointer to more information | …] 

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c] 
* … --> 
<!-- numbers of pros and cons can vary -->

