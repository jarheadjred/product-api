# Evaluate and decide on the NoSql DataStore to utilize for local source data

* Status: [exploring] <!-- when final decision made edit here or new ADR | … | superseded by [ADR-0005](0005-example.md)]  optional -->
* Deciders: Merchandising and Assortment Business, Stores Director Engineering, Specific team via SEM
* Date: 2020-05-06

Technical Story: https://gitlab.com/jarheadjred/product-api/-/issues/10 (chicken and egg, story placeholder depends on ADR decision)

## Context and Problem Statement

The My Retail Product API has NoSql (specifically document storage needs) data storage needs as the system of record for certain product information fragments. A datastore needs to be selected.

## Decision Drivers <!-- optional -->

* Application DataStore sla requirements
* Consistency Requirements of the application data administration etc.
* Organizational support capabilities
* Access and API capabilities (key lookup only? is search on field or index needed? etc.)

## Considered Options

* EnterpriseRedis
* MongoDB
* Json-B in RDBMS ? 
* … <!-- numbers of options can vary -->

## Decision Outcome

No Decision yet, still in research and evaluation phase of Decision Record

### Positive Consequences <!-- optional -->

* [e.g., improvement of quality attribute satisfaction, follow-up decisions required, …]
* …

### Negative Consequences <!-- optional -->

* [e.g., compromising quality attribute, follow-up decisions required, …]
* …

## Pros and Cons of the Options <!-- optional -->

### [option 1]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

### [option 2]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

### [option 3]

[example | description | pointer to more information | …] <!-- optional -->

* Good, because [argument a]
* Good, because [argument b]
* Bad, because [argument c]
* … <!-- numbers of pros and cons can vary -->

## Links <!-- optional -->

* [Link type] [Link to ADR] <!-- example: Refined by [ADR-0005](0005-example.md) -->
* … <!-- numbers of links can vary -->
